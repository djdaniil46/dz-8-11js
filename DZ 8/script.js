"use strict";

const price = document.getElementById("price");

let span = document.createElement("span");
span.innerText = "";
span.setAttribute(
  "style",
  "transition:opacity 0.4s, easy-ease; background-color:red; opacity: 0; display:flex; justify-content:center;align-items:center;height:50px;border-radius:3px; padding:4px; font-family:sans-serif; text-transform:uppercase;"
);
const close = document.createElement("div");
close.innerHTML = "&#10006;";
close.setAttribute(
  "style",
  "transition:opacity 0.4s, easy-ease; background-color:red; opacity: 0;width:fit-content; border-radius:3px; padding:4px; top:20px; font-size:15px; cursor:pointer; margin:5px auto;"
);

let sorry = document.createElement("p");
sorry.textContent = "Please enter correct price";
sorry.style.color = "red";
sorry.style.opacity = 0;

document.body.prepend(span);
span.after(sorry);
span.after(close);

price.addEventListener("focus", (event) => {
  price.style.cssText =
    "transition:border 0.2s,easy-ease; border: 3px solid greenyellow; border-radius:3px;";
  if (+price.value <= 0) {
    span.style.opacity = "0";
    close.style.opacity = "0";
    sorry.style.opacity = 0;
  }
});

price.addEventListener("blur", (event) => {
  if (+price.value <= 0 || isNaN(price.value)) {
    price.style.cssText =
      "transition:border 1s,easy-ease; border: 3px solid red;";
    price.value = "";
    sorry.setAttribute(
      "style",
      "transition:opacity 0.4s, easy-ease; background-color:red; opacity: 0; display:flex; justify-content:center;align-items:center;height:50px;border-radius:3px; padding:4px; font-family:sans-serif; text-transform:uppercase;"
    );
    sorry.style.opacity = 1;
    close.style.opacity = 1;
    close.style.right = "32%";
  } else {
    price.style.cssText =
      "transition:border 1s,easy-ease; border: 3px solid greenyellow; border-radius:3px;";
    span.innerText = `Текущая цена: ${price.value}`;
    span.style.left = "45%";
    span.style.backgroundColor = "greenyellow";
    span.style.opacity = 1;
    close.style.opacity = 1;
    close.style.right = "38%";
  }

  close.addEventListener("click", (disappear) => {
    close.style.opacity = 0;
    span.style.opacity = 0;
    sorry.style.opacity = 0;
  });
  console.log(price.value);
});
